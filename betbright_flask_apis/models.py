from flask import Flask
from marshmallow import Schema, fields, pre_load, validate
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
# from app import db
from datetime import datetime
from config import db

ma = Marshmallow()


class Match(db.Model):
    """ Match Model for storing match related details """
    __tablename__ = "match"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    url = db.Column(db.String(255), unique=True, nullable=False)
    name = db.Column(db.String(50), unique=True)
    startTime = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)
    sports = db.relationship("Sport", backref='match', lazy=True)

    def __repr__(self):
        return "<Match '{}'>".format(self.name)


class Sport(db.Model):
    """ """
    __tablename__ = "sport"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(50), unique=True)
    markets = db.relationship('Market', backref='market', lazy=True) 

    match_id = db.Column(db.Integer, db.ForeignKey('match.id'))

    def __repr__(self):
        return "<Sport '{}'>".format(self.name)


class Market(db.Model):
    """docstring for Market"""
    __tablename__ = "market"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(50), unique=True)

    selection_id = db.Column(db.Integer, db.ForeignKey('selection.id'))    
    selection = db.relationship(
                'Selection', 
                backref=db.backref('market', lazy='dynamic'),
                )

    sport_id = db.Column(db.Integer, db.ForeignKey('sport.id'))

    def __repr__(self):
        return "<Market '{}'>".format(self.name)
        

class Selection(db.Model):
    __tablename__ = "selection"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(50), unique=True)
    odds = db.Column(db.Float)

    def __repr__(self):
        return "<Selection '{}'>".format(self.name)


##### SCHEMAS #####


class SportSchema(ma.Schema):
    id = fields.Integer()
    name = fields.String()
    market_id = fields.Integer()


class MatchSchema(ma.Schema):
    id = fields.Integer()
    url = fields.String(required=True)
    name = fields.String(required=True)
    startTime = fields.DateTime()
    # sports = fields.Nested(SportSchema)
    sports = fields.Integer()


class SelectionSchema(ma.Schema):
    """docstring for SelectionSchema"""
    id =fields.Integer()
    name = fields.String()
    odds = fields.Float()


class MarketSchema(ma.Schema):
    id = fields.Integer()
    name = fields.String()
    selection = fields.Nested(SelectionSchema)





