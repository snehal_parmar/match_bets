import os
from config import db
# from models import Person
from models import Match
from models import MatchSchema 
from models import Sport
from models import SportSchema
from models import Selection 
from models import SelectionSchema 
from models import Market
from models import MarketSchema 
from settings import SQLITE_DB_FILE


# Data to initialize database with
# PEOPLE = [
#     {'fname': 'Doug', 'lname': 'Farrell'},
#     {'fname': 'Kent', 'lname': 'Brockman'},
#     {'fname': 'Bunny','lname': 'Easter'}
# ]


# Delete database file if it exists currently

if os.path.exists(SQLITE_DB_FILE):
    os.remove(SQLITE_DB_FILE)


# Create the database
db.create_all()


# Iterate over the PEOPLE structure and populate the database
# for person in PEOPLE:
#     p = Person(lname=person['lname'], fname=person['fname'])
#     db.session.add(p)

db.session.commit()