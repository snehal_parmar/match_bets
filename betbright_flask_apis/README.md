
================================================
    README.md
================================================

1) Setup the virturalenv using following command:
    # source betbright_flask_apis/venvApi/bin/activate

2) Config the sqlite db file by editing setting.py

3) Create the db using following command: 
    # python build_database.py

4) Run the api server:
    # python api.py

5) Open the url in browser: 
    url: http://127.0.0.1:5000/apidocs

