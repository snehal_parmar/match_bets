from flask import Flask, jsonify, request
from flasgger import Swagger
from models import Match
from models import MatchSchema 
from models import Sport
from models import SportSchema
from models import Selection 
from models import SelectionSchema 
from models import Market
from models import MarketSchema 
from flask_sqlalchemy import SQLAlchemy
import os
from config import app, db
from jsonschema import ValidationError


match_schema = MatchSchema()
matches_schema = MatchSchema(many=True)
sport_schema = SportSchema()
sports_schema = SportSchema(many=True)
market_schema = MarketSchema()
markets_schema = MarketSchema(many=True)
selection_schema = SelectionSchema()
selections_schema = SelectionSchema(many=True)


@app.route('/api/match/<sport>')
def get_matches_sport(sport):
    """Endpoint for getting matches for given sport
    sport is string parameter
    ---
    tags: [Match]
    parameters:
        - name: sport
          in: query
          type: string
    definitions:
        Match:
            type: object
            properties:
            Sports:
                type: array
                items:
                sports:
                    type: string
    responses:
        200:
            description: "Returns the list of matches for sport"
    """
    matches = Match.query.filter(Match.name == sport)
    matches_data = matches_schema.dump(list(matches))
    return jsonify({'matches': matches_data})


@app.route('/api/match', methods=['GET'])
def get_matches():    
    """Endpoint returning a list of Matches
    This is using docstrings for specifications.
    ---
    tags: [Match]
    parameters:
        - name: ordering
          in: query
          type: string
          default: startTime
        #   enum: ['startTime', 'name']
        - name: name
          in: query
          type: string

    responses:
      200:
        description: A list of Matches
        # examples:
        #   rgb: ['red', 'green', 'blue']
    """
    req_args = request.args
    ordering = req_args.get('ordering')
    name = req_args.get('name')
    print(ordering)
    if ordering:
        matches = Match.query.order_by(ordering).all()
    elif name:
        matches = Match.query.filter(Match.name==name)
    else:
        matches = Match.query.all()
        
    matches_data = matches_schema.dump(list(matches))
    return jsonify({'matches': matches_data})


@app.route('/api/match/<id>')
def get_match(id):    
    """Example endpoint returning a list of Match by id
    This is using docstrings for specifications.
    ---
    tags: [Match]
    parameters:
      - name: id
        in: path
        type: string
        required: true
    responses:
      200:
        description: GET match for id
    """
    match = Match.query.filter(Match.id == id).first()
    match_data = match_schema.dump(match)
    return jsonify({'Matches': match_data})    


@app.route('/api/match', methods=['POST'])
def new_match():
    """
    Creating new Match 
    Docstring
    ---
    info: "creating new Match resource"
    tags: [Match]
    parameters: [
      {
        in: body,
        name: body,
        example: {
                    url: testing.com, 
                    name: NameTest
                },
        description: "Match object that needs to be added to the store",
        required: false,
      }
    ]
    responses:
        200:
            description: Create new Match  
    """
    json_data = request.get_json()
    if not json_data:
        return jsonify({'message': 'No input data provided'}), 400

    # Validate and deserialize input
    try:
        unmarshal_data = match_schema.load(json_data)
        data = unmarshal_data.data
    except ValidationError as err:
        return jsonify(err), 422
    url, name = data['url'], data['name']
    match = Match.query.filter_by(url=url, name=name).first()
    
    if match is None:
        # Create a new match
        match = Match(url=url, name=name)
        db.session.add(match)
        db.session.commit()

    match_data = match_schema.dump(match)
    # import ipdb; ipdb.set_trace()
    return jsonify({"message": "Created new",
        "match": match_data})


@app.route('/api/match/<id>', methods=['PUT'])
def update_match(id):
    """
    Updating Match details
    Updating the match object when new event is publish
    ---
    info: "Update the match details using PUT"
    tags: [Match]
    
    """
    json_data = request.get_json()
    if not json_data:
        return jsonify({'message': 'No input data provided'}), 400

    # Validate and deserialize input
    try:
        unmarshal_data = match_schema.load(json_data)
        data = unmarshal_data.data
    except ValidationError as err:
        return jsonify(err), 422
    id = data['id']
    match = Match.query.filter_by(id=url).first()

    import ipdb; ipdb.set_trace()
    pass


@app.route('/api/match/publish/event', methods=['PUT'])
def event_update_match():
    """
    Endpoint to update the match details on publishing of new event
    ---
    tags: [Match]
    parameters: [
      {
        in: body,
        name: body,
        example: {
                "id": 8661032861909884224,
                "message_type": "UpdateOdds",
                "event": {
                    "id": 994839351740,
                    "name": "Real Madrid vs Barcelona",
                    "startTime": "2018-06-20 10:30:00",
                    "sport": {
                        "id": 221,
                        "name": "Football"
                    },
                },
                
                "markets": [
                    {
                    "id": 385086549360973392,
                    "name": "Winner",
                    "selections": [
                        {
                            "id": 8243901714083343527,
                            "name": "Real Madrid",
                            "odds": 10.00
                        }, 
                        {
                            "id": 5737666888266680774,
                            "name": "Barcelona",
                            "odds": 5.55
                        } ]
                    } 
                ]
            },
        description: "Match object that needs to be added to the store",
        required: false,
      }
    ]
    responses:
        200:
            description: "Return updated match details"
    """
    # todo update the match based on the event json
    # todo validate the event json and get the match and id and get match object from db
    # todo update the match db object with json event's selection data
    pass


@app.route('/api/sport', methods=['GET'])
def get_sports():
    """Example endpoint returning a list of Sports
    This is using docstrings for specifications.
    ---
    tags: [Sport]
    responses:
      200:
        description: A list of Sports
    """
    sports = Sport.query.all()
    sports_data = sports_schema.dump(list(sports))
    return jsonify({'Sports': sports_data})


@app.route('/api/sport/<id>', methods=['GET'])
def get_sport(id):
    """Example endpoint returning a list of Sport by id
    This is using docstrings for specifications.
    ---
    tags: [Sport]
    parameters:
      - name: id
        in: path
        type: interger
        required: true
    definitions:
      Match:
        type: object
        properties:
          Sports:
            type: array
            items:
              $ref: '#/definitions/sports'
            sports:
                type: string
    responses:
      200:
        description: GET Sport for id
    """
    sport = Sport.query.filter(Sport.id == id).first()
    sport_data = sport_schema.dump(list(sport))
    return jsonify({'Sport': sport_data})


@app.route('/api/sport', methods=['POST'])
def new_sport():
    """
    Creating new Sport 
    Docstring
    ---
    info: "creating new Sport resource"
    tags: [Sport]
    parameters: [
      {
        in: body,
        name: body,
        example: {
                    name: NameTest,
                    market: 1
                },
        description: "Sport object that needs to be added to the store",
        required: false,
      }
    ]
    responses:
        200:
            description: Create new Sport  
    """
    json_data = request.get_json()
    if not json_data:
        return jsonify({'message': 'No input data provided'}), 400

    # Validate and deserialize input
    try:
        unmarshal_data = sport_schema.load(json_data)
        data = unmarshal_data.data
    except ValidationError as err:
        return jsonify(err), 422
    name, match_id = data['name'], data['match_id']
    sport = Sport.query.filter_by(name=name).first()
    
    if sport is None:
        # Create a new match
        sport = Sport(name=name, match_id=match_id)
        db.session.add(sport)
        db.session.commit()

    sport_data = sport_schema.dump(sport)
    return jsonify({"message": "Created new",
        "match": sport_data})

# @app.route('/api/sport', methods=['PUT'])


@app.route('/api/market', methods=['GET'])
def get_markets():
    """Example endpoint returning a list of Markets
    This is using docstrings for specifications.
    ---
    tags: [Market]
    responses:
      200:
        description: A list of Markets
    """
    markets = Market.query.all()
    markets_data = markets_schema.dump(list(markets))
    return jsonify({'Markets': markets_data})


@app.route('/api/market/<id>', methods=['GET'])
def get_market(id):
    """Example endpoint returning a list of Market by id
    This is using docstrings for specifications.
    ---
    tags: [Market]
    parameters:
      - name: id
        in: path
        type: interger
    definitions:
      Market:
        type: object
        properties:
          Nane: 
            type: string
          Selection:
            type: array
            items:
              name: string

    responses:
      200:
        description: GET Market for id
    """
    # market = Market.query.filter(Market.id == id).first()
    market = Market.query.get(id)
    market_data = market_schema.dump(market)
    selection_data = selection_schema.dump(market.selection.all())
    return jsonify({'Market': market_data, 'selection': selection_data})


@app.route('/api/market', methods=['POST'])
def new_market():
    """
    Creating new Market 
    Docstring
    ---
    info: "creating new Market resource"
    tags: [Market]
    parameters: [
      {
        in: body,
        name: body,
        example: {
                    name: marketName,
                    selection_id: 8243901714083343527
                },
        description: "Market object that needs to be added to the store",
        required: false,
      }
    ]
    responses:
        200:
            description: Create new Market  
    """
    json_data = request.get_json()
    if not json_data:
        return jsonify({'message': 'No input data provided'}), 400
    selection_id = json_data.get('selection_id')   
    selection = Selection.query.filter_by(id=selection_id).first()
    if selection is None:
        # Create new selection
        selection = Selection(id=selection_id, name=None, odds=None)
        db.session.add(selection)
        db.session.commit()
    selection_data = selection_schema.dump(selection)
    json_data.update({'selection': selection_data.data})

    # Validate and deserialize input
    try:
        unmarshal_data = market_schema.load(json_data)
        data = unmarshal_data.data
    except ValidationError as err:
        return jsonify(err), 422
    name = data['name']
    market= Market.query.filter_by(name=name).first()
    
    if market is None:
        # Create a new Market
        market = Market(name=name, selection=selection)
        db.session.add(market)
        db.session.commit()

    market_data = market_schema.dump(market)
    return jsonify({"message": "Created new",
        "market": market_data})

# @app.route('/api/market', methods=['PUT'])


@app.route('/api/selection', methods=['GET'])
def get_selections():
    """Endpoint returning a list of Selection
    This is using docstrings for specifications.
    ---
    tags: [Selection]
    responses:
      200:
        description: A list of Selection
    """
    selections = Selection.query.all()
    selections_data = selections_schema.dump(list(selections))
    return jsonify({'Selections': selections_data})


@app.route('/api/selection/<id>', methods=['GET'])
def get_selection(id):
    """Endpoint returning a list of Selection by id
    This is using docstrings for specifications.
    ---
    tags: [Selection]
    parameters:
      - name: id
        in: path
        type: interger
        required: true
    definitions:
      Selection:
        type: object
        properties:
          Name: 
            type: string
          Selection:
            type: array
            items:
              name: string

    responses:
      200:
        description: GET Selection for id
    """
    selection = Selection.query.filter(Selection.id == id).first()
    selection_data = selection_schema.dump(selection)
    return jsonify({'Selection': selection_data})


@app.route('/api/selection', methods=['POST'])
def new_selection():
    """
    Creating new Selection 
    Docstring
    ---
    info: "creating new Selection resource"
    tags: [Selection]
    parameters: [
      {
        in: body,
        name: body,
        example: {
                    name: NameTest,
                    odds: 1.01,
                    market: newMarket
                },
        description: "Selection object that needs to be added to the store",
        required: false,
      }
    ]
    responses:
        200:
            description: Create new Selection  
    """
    json_data = request.get_json()
    if not json_data:
        return jsonify({'message': 'No input data provided'}), 400
    market_name = json_data.get('market')   
    market = Market.query.filter_by(name=market_name).first()
    if market is None:
        # Create new market
        market = Market(name=market_name)
        db.session.add(market)
        db.session.commit()
    market_data = market_schema.dump(market)
    json_data.update({'market': market_data.data})

    # Validate and deserialize input
    try:
        unmarshal_data = selection_schema.load(json_data)
        data = unmarshal_data.data
    except ValidationError as err:
        return jsonify(err), 422
    name, odds = data['name'], data['odds']
    selection= Selection.query.filter_by(name=name).first()
    
    if selection is None:
        # Create a new selection
        selection = Selection(name=name, odds=odds, market=market)
        db.session.add(selection)
        db.session.commit()

    selection_data = selection_schema.dump(selection)
    return jsonify({"message": "Created new",
        "selection": selection_data})
 

# @app.route('/api/selection', methods=['PUT'])


if __name__ == '__main__':
    app.run(debug=True)

