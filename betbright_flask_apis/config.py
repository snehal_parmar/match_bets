import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from settings import SQLITE_DB_FILE
from flasgger import Swagger

basedir = os.path.abspath(os.path.dirname(__file__))


# # Create the Connexion application instance
# connex_app = connexion.App(__name__, specification_dir=basedir)


# # Get the underlying Flask app instance
# app = connex_app.app

app = Flask(__name__)
swagger = Swagger(app)

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

# Configure the SQLAlchemy part of the app instance
app.config['SQLALCHEMY_ECHO'] = True

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////' + os.path.join(basedir, SQLITE_DB_FILE)



# Create the SQLAlchemy db instance
db = SQLAlchemy(app)


# Initialize Marshmallow
ma = Marshmallow(app)



# app = Flask(__name__)
# swagger = Swagger(app)

# app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True

# app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////' + os.path.join(basedir, SQLITE_DB_FILE)
# db = SQLAlchemy(app)